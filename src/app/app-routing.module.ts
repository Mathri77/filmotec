import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FilmdetailsComponent } from './filmdetails/filmdetails.component';
import { ListeFilmComponent } from "./liste-film/liste-film.component"
import { ListePersonnelComponent } from "./liste-personnel/liste-personnel.component"

const routes: Routes = [
  { path: '', component: ListeFilmComponent },
  { path: 'liste', component: ListePersonnelComponent },
  { path: 'details/:id', component: FilmdetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
