import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FilmService } from '../film.service';
import { Favoris } from '../favoris';
import { FavorisService } from '../favoris.service';
import { FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-filmdetails',
  templateUrl: './filmdetails.component.html',
  styleUrls: ['./filmdetails.component.scss']
})
export class FilmdetailsComponent implements OnInit {

  Film: any[number] = [];
  favoris: Favoris[];
  commForm!: FormGroup;
        isSubmitted = false;
  submitted = false;
  tabComment!: string;
  comment: any[] = [];
  noteForm!: FormGroup;
  tabNote!: string;
  not: any[] = [];


  constructor(public film: FilmService, private route: ActivatedRoute, private favori: FavorisService) {

    this.favoris = this.favori.getAllFavoris();

    this.commForm = new FormGroup({
      commentaire: new FormControl(''),
    });

    this.noteForm = new FormGroup({
      note: new FormControl(''),
    })
  }

  ngOnInit(): void {
    this.getDetailFilm(this.route.snapshot.params.id);
    this.getCommentaire();
    this.getNote();
  }

  getDetailFilm(id: number) {
    this.film.getDetailFilm(id).subscribe((data: Object) => {
      this.Film = [data];
    });
  }

  getFavori() {
    let id: number;
    let image: string = "";
    let nom: string = "";

    this.Film.forEach((element: { id: number; poster_path: string; title: string; }) => {
      id = element.id;
      image = element.poster_path;
      nom = element.title;
      let lefavori = new Favoris(id, image, nom);
      this.favori.addFavoris(lefavori);
    });
  }

  commentaire() {
    this.isSubmitted = true;
    if (this.commForm.value.commentaire == '') {
      return;
    } else {
      this.tabComment = JSON.stringify(this.commForm.value.commentaire);
      localStorage.setItem('Commentaire', this.tabComment);
    }
  }

  getCommentaire(){
    this.comment = JSON.parse(localStorage.getItem('Commentaire') || '[]');
    return this.comment;
  }

  note(){
    this.submitted = true;
    if (this.noteForm.value.note == '') {
      return;
    } else {
      this.tabNote = JSON.stringify(this.noteForm.value.note);
      localStorage.setItem('Note', this.tabNote);
    }
  }

  getNote(){
    this.not = JSON.parse(localStorage.getItem('Note') || '[0]');
    return this.not;
  }
}