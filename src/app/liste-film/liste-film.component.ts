import { Component, OnInit } from '@angular/core';
import { FilmService } from '../film.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-liste-film',
  templateUrl: './liste-film.component.html',
  styleUrls: ['./liste-film.component.scss']
})
export class ListeFilmComponent implements OnInit {

  constructor(public film: FilmService) { }

  Films: any[number]=[];
  identForm!: FormGroup;
  isSubmitted = false;

  ngOnInit(): void {
    this.identForm = new FormGroup({
      refilm: new FormControl(''),
    });
    this.getFilms();
  }

  getFilms(){
    this.film.getFilm().subscribe((data: any) => {
      this.Films = data;
    })
  }

  getRechercheFilm(refilm : string){
    this.film.getRechercheFilm(refilm).subscribe((data: any) => {
      this.Films = data;
    })
  }

  recherche(){
    this.isSubmitted = true;
    if (this.identForm.value.refilm == ''){
      return;
    }else{
      this.getRechercheFilm(this.identForm.value.refilm);
      return;
    }
  }

}
